// -*- compile-command: "g++ -g Puissance4.cc" -*-

#include <iostream>
using namespace std;
	       
//fonction qui change l'affichage des points
void aff_pion(int val){
  if(val==1){
    cout << " X ";
  }
  else if(val==2){
    cout << " O ";
  }
  else{
    cout << "   ";
  }
}

//fonction qui affiche la grille
void afficher_grille(int grille[6][7])
{
  for (int i=0;i<6;++i) //on parcout toutes les lignes et toutes les colonnes
    {
      cout << "----------------------------"<<endl;
      for (int j=0;j<7;++j)
      {
 	      cout << '|' ;
        aff_pion(grille[i][j]);
      }
      cout << '|' << endl ;  
    }
  cout << "----------------------------"<<endl;
  
  for(int k=0;k<7;k++){ //on affiche le numéro des colonnes
    cout << "  " << k << ' ';
  }
  cout <<endl;
}

//fonction qui place un pion dans la colonne où le joueur souhaite jouer en vérifiant qu'elle soit dans la zone du jeu 
//et qu'elle ne soit pas déjà pleine
void jouer(int grille[6][7], int &a, int &b, int jo) //on veut récupérer la ligne a et la colonne b où le pion est posé 
{
  int M=-1;
  while (M<0 || M>6 || grille[0][M]==1 || grille[0][M]==2) //on redemande une colonne tant qu'elle n'est pas bonne
    {
      cout << "Choisissez la colonne " << endl;
      cin >> M;
      if (M<0 || M>6)
      {
        cout << "Erreur, la colonne " << M << " n'existe pas" << "\n\r" << endl;
      }
      if (grille[0][M]==1 || grille[0][M]==2)
      {
        cout << "Erreur la colonne " << M << " est pleine !" << "\n\r" << endl;
      }
    }
  int i=5;
  while(grille[i][M]!=0) //on place le pion dans la colonne dès qu'il y a une ligne vide en parcourant les lignes du bas vers le haut
    {
      i=i-1;
    }
  grille[i][M]=jo;
  a=i;
  b=M;
}

//fonction qui renvoie le nombre de voisins identiques dans la ligne du dernier pion posé par le joueur jo
int voisins_ligne(int grille[6][7], int a, int b, int jo)
{

  int compt=0;
  int j=b;
  while(j<=6 && grille[a][j]==jo) //on regarde les colonnes à droite du pion
    {
      j=j+1;
      compt=compt+1;
    }
  if (compt<4) //si il y a moins de 4 voisins à droite, on regarde les colonnes à gauche, sinon on s'arrête car le joueur a déjà gagné
    {
       j=b-1;
      while(j>=0 && grille[a][j]==jo)
      {  
       j=j-1;
       compt=compt+1;
      }
    }
  return compt;
}

//fonction qui renvoie le nombre de voisins identiques dans la colonne du dernier pion posé par le joueur jo
int voisins_colonne(int grille[6][7],int a, int b, int jo)
{
  int compt=0;
  int j=a;
  while(j<=5 && grille[j][b]==jo) //on regarde les pions qui sont en dessous du dernier posé
    {
      j=j+1;
      compt=compt+1;
    }
  return compt;
}

//fonction qui renvoie le nombre de voisins identiques dans les diagonales du dernier pion posé par le joueur jo
int voisins_diago(int grille[6][7], int a, int b, int jo)
{
  int compt=0;
  int i=a;
  int j=b;
  while(j<=6 && i>=0 && grille[i][j]==jo) //on vérifie la diagonale à droite vers le haut
    {
      i=i-1;
      j=j+1;
      compt=compt+1;
    }
  i=a+1;
  j=b-1;
  while(j>=0 && i<=5 && grille[i][j]==jo) //on vérifie la diagonale à gauche vers le bas
    {
      i=i+1;
      j=j-1;
      compt=compt+1;
    }
  if (compt==4) //on s'arrête si la première diagonale contient 4 pions alignés
    {
      return compt;
    }
  compt=0;
  i=a;
  j=b;
  while(j>=0 && i>=0 && grille[i][j]==jo) //on vérifie la diagonale à gauche vers le haut
    {
      i=i-1;
      j=j-1;
      compt=compt+1;
    }
  i=a+1;
  j=b+1;
  while(j<=6 && i<=5 && grille[i][j]==jo) //on vérifie la diagonale à droite vers le bas
    {
      i=i+1;
      j=j+1;
      compt=compt+1;
    }
  return compt;
}

//fonction qui renvoie True si il y a 4 voisins identiques alignés, False sinon
bool gagner(int grille[6][7],int a, int b,int jo)
  {
    return (voisins_ligne(grille, a, b, jo) < 4 && voisins_colonne(grille, a, b, jo) < 4 && voisins_diago(grille, a, b, jo) < 4);
  }

int main () {
  int g[6][7];
  for (int i=0;i<6;++i) //on crée une grille vide
  {
      for (int j=0;j<7;++j)
    	{
        g[i][j]=0;
	}
  }
  int a=-1; //numéro de la ligne
  int b=-1; //numéro de la colonne
  int jo=1; //numéro du joueur
  int joueur_suivant=1; //numéro du joueur qui joue au tour suivant
  afficher_grille(g);
  cout << "\n\r" << endl;
  cout << "Début de la partie : " << endl;
  cout << "Le joueur 1 joue avec les pions X et le joueur 2 avec les pions O \n\r" << endl;
  while (gagner(g, a, b, jo)) //on continue la partie tant que le dernier joueur qui a joué n'a pas gagné
    {
      jo=joueur_suivant; //on change de joueur
      if (jo==1)
      {
        cout << "C'est au tour du joueur 1" << endl;
        jouer(g,a,b,jo); //le joueur joue
        afficher_grille(g); //on affiche la grille avec le nouveau pion
        joueur_suivant=2;
	      cout << "\n\r" << endl;
      }
      else
      {
        cout << "C'est au tour du joueur 2" << endl;
        jouer(g,a,b,jo);
        afficher_grille(g);
        joueur_suivant=1;
        cout << "\n\r" << endl;
      }
    }
  cout << "Bravo le joueur " << jo << " a gagné" << endl;
  cout << "Fin de la partie" << endl;
}
